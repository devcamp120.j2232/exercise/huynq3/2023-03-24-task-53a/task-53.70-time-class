package com.j2232;
import java.text.SimpleDateFormat;
import java.util.*;
public class Time {
    private int hour;
    private int minute;
    private int second;

    public Time(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }
    public void setTime(int hour, int minute, int second){
        if (hour<0||hour>23||minute<0||minute>59||second<0||second>59){
            System.out.println("Nhập sai giờ- phút- giây gì đó rồi bạn êi");
        }
        else {
            this.hour=hour;
            this.minute=minute;
            this.second=second;
        }
    }
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss"); 
        String timeFormated = formatter.format(new java.util.Date(0, 0, 0,this.hour, this.minute, this.second));
        return timeFormated;
    }

    public int nextSecond(){
        if (this.second ==59){
            this.minute= minute+1;
            this.second=0;
            
        }
        else {
        this.second=second+1;
        }
        return second;
    }
    public int previousSecond(){
        if (this.second ==1){
            this.minute= minute-1;
            this.second=59;
            
        }
        else {
        this.second=second-2;
        }
        return second;
    }
}
