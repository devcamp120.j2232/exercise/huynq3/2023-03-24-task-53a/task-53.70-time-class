import com.j2232.Time;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Time time1= new Time(11, 30, 59);
        time1.setTime(11, 30, 59);
        System.out.println("Time 1: "+time1);
        Time time2= new Time(9, 15, 1);
        time2.setTime(9, 15, 1);
        System.out.println("Time 2: "+time2);

        System.out.println("----------Thử sai phía dưới----------");
       
        Time time3= new Time(24, 60, 61);
        time3.setTime(24, 60, 61);
        System.out.println(time3);
        
        System.out.println("----------sub4: +2 second vào time1 và trừ 2 second vào time2----------");
        time1.nextSecond();
        time2.previousSecond();
        System.out.println("New Time 1: "+time1);
        System.out.println("New Time 2: "+time2);



    }
}
